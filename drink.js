class Drink{
    _id;
    _maNuocUong;
    _tenNuocUong;
    _donGia;
    _ngayTao;
    _ngayCapNhat;

    constructor(paramId,paramMa,paramTen,paramDonGia,paramNgayTao,paramNgayCapNhat)
    {
        this._id=paramId;
        this._maNuocUong=paramMa;
        this._tenNuocUong=paramTen;
        this._donGia=paramDonGia;
        this._ngayTao=paramNgayTao;
        this._ngayCapNhat=paramNgayCapNhat;
    }

}
module.exports = {Drink};