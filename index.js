const {Drink}=require('./drink.js')
//import thu vien express
const express= require('express');
//Khoi tao app nodejs bang express
const app= new express();

//Khai bao ung dung chay tren cong 8000
const port = 8000;

//Cấu hình để sử dụng json
app.use(express.json());

const drink1= new Drink(1,"TRATAC","Trà tắc",10000,"14/5/2021","14/5/2021");
const drink2= new Drink(2,"COCA","Cocacola",15000,"14/5/2021","14/5/2021");
const drink3= new Drink(3,"PEPSI","Pepsi",15000,"14/5/2021","14/5/2021");

var object1={
    id:4,
    maNuocUong:"TRATAC",
    tenNuocUong:"Trà tắc",
    gia:10000,
    ngayTao:"14/5/2021",
    ngayCapNhat: "14/5/2021"
}
var object2={
    id:5,
    maNuocUong:"COCA",
    tenNuocUong:"Cocacola",
    gia:15000,
    ngayTao:"14/5/2021",
    ngayCapNhat: "14/5/2021"
}

var object3={
    id:6,
    maNuocUong:"PEPSI",
    tenNuocUong:"Pepsi",
    gia:15000,
    ngayTao:"14/5/2021",
    ngayCapNhat: "14/5/2021"
}

const arrayDrinkObj=[object1,object2,object3];

const arrayDrinks=[drink1,drink2,drink3];
//Khai bao API tra ve chuoi
app.get("/drinks-class", (request,response) =>{
   response.send(arrayDrinks);
})

//Khai bao API tra ve object drink
app.get("/drinks-object", (request,response) =>{
    response.send(arrayDrinkObj);
 })

//Chay ung dung
app.listen(port, () =>{
    console.log("Ung dung dang chay");
})